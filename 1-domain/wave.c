#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <bcl.h>

#include "domain.h"

void
process(float tx, float ax, float ty, float ay, char* ims_name, char* imd_name){

    pnm ims = pnm_load(ims_name);

    int cols = pnm_get_width(ims);
    int rows = pnm_get_height(ims);

    pnm imd = pnm_new(cols, rows, PnmRawPpm);

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++){
            float x = (float) j;
            float y = (float) i;

            float x_src = x + ax * sin((2 * pi * y) / tx);
            float y_src = y + ay * sin((2 * pi * x) / ty);

            for(int k = 0; k < 3; k++){
                pnm_set_component(imd, i, j, k, bilinear_interpolation(x_src, y_src, ims, k));
            }
        }
    }

    pnm_save(imd, PnmRawPpm, imd_name);

    pnm_free(ims);
    pnm_free(imd);

}

static void usage(char* s){
    fprintf(stderr,"%s <tx> <ax> <ty> <ay> <ims> <imd>\n",s);
    exit(EXIT_FAILURE);
}

#define PARAM 6
int main(int argc, char* argv[]){

    if(argc != PARAM+1) usage(argv[0]);

    float tx = atoi(argv[1]);
    float ax = atoi(argv[2]);
    float ty = atoi(argv[3]);
    float ay = atoi(argv[4]);

    char* ims_name = argv[5];
    char* imd_name = argv[6];

    process(tx, ax, ty, ay, ims_name, imd_name);
    

    return EXIT_SUCCESS;
}