#include <stdlib.h>
#include <stdio.h>
#include <pnm.h>


pnm
transform_v(pnm ims){

    int cols = pnm_get_width(ims);
    int rows = pnm_get_height(ims);

    pnm imd = pnm_new(cols, rows, PnmRawPpm);

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++){
            for(int k = 0; k < 3; k++){
                pnm_set_component(imd, i, j, k, pnm_get_component(ims, rows - i - 1, j, k));
            }
        }
    }

    return imd;
}

pnm
transform_h(pnm ims){
    int cols = pnm_get_width(ims);
    int rows = pnm_get_height(ims);

    pnm imd = pnm_new(cols, rows, PnmRawPpm);

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++){
            for(int k = 0; k < 3; k++){
                pnm_set_component(imd, i, j, k, pnm_get_component(ims, i, cols - j - 1, k));
            }
        }
    }

    return imd;
}

pnm
transform_t(pnm ims){
    int cols = pnm_get_width(ims);
    int rows = pnm_get_height(ims);

    pnm imd = pnm_new(rows, cols, PnmRawPpm);

    for(int i = 0; i < cols; i++){
        for(int j = 0; j < rows; j++){
            for(int k = 0; k < 3; k++){
                pnm_set_component(imd, i, j, k, pnm_get_component(ims, j, i, k));
            }
        }
    }

    return imd;
}

void 
optimize_orders(char* dir, int* t, int* h, int* v){
    int i = 0;

    while(dir[i] != 0){

        int t_temp = *t;
        int h_temp = *h;
        int v_temp = *v;

        switch (dir[i]) {
            case 't':
                *t = !(t_temp);
                *h = v_temp;
                *v = h_temp;
                break;
            case 'h':
                *h = !(h_temp);
                break;
            case 'v':
                *v = !(v_temp);
                break;
            default:
                printf("Wrong input. Directions can only be 't', 'h' or 'v'.\nExisting now...\n");
                exit(EXIT_FAILURE);
        }
        i++;
    }
}


void 
process(char* dir, char* ims_name, char* imd_name)
{

    pnm imd = pnm_load(ims_name);

    int t = 0;
    int h = 0; 
    int v = 0;

    optimize_orders(dir, &t, &h, &v);

    printf("Optimized orders : t=%d, h=%d, v=%d\n", t, h, v);

    pnm res;
    
    if(t) {
        res = transform_t(imd);
        pnm_free(imd);
        imd = res;
    }

    if(h) {
        res = transform_h(imd);
        pnm_free(imd);
        imd = res;
    }

    if(v) {
        res = transform_v(imd);
        pnm_free(imd);
        imd = res;
    }

    pnm_save(imd, PnmRawPpm, imd_name);
    pnm_free(imd);
}

void
usage(char* s)
{
    fprintf(stderr,"%s <dir> <ims> <imd>\n",s);
    exit(EXIT_FAILURE);
}

#define PARAM 3
int
main(int argc, char* argv[])
{
    if(argc != PARAM+1) usage(argv[0]);

    char* dir = argv[1];
    char* ims_name = argv[2];
    char* imd_name = argv[3];

    process(dir, ims_name, imd_name);

    return EXIT_SUCCESS;
}
