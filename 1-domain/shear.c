#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <bcl.h>

#include "domain.h"

typedef enum Orientation {
    Right, Right_flip, Left, Left_flip
} Orientation;

Orientation
compute_orientation(float angle){
    if(angle < -90){ // -180 < angle < -90
        return Left_flip;
    } else if(angle < 0){ // -90 < angle < 0
        return Left;
    } else if(angle < 90){ // 0 < angle < 90
        return Right;
    } else { // 90 < angle < 180
        return Right_flip;
    }
}

void 
new_image_dimension(char dir, float angle, 
                    int old_rows, int old_cols, 
                    int* new_rows, int* new_cols){
    
    float h = (float) old_rows;
    float w = (float) old_cols;
    
    float angle_rad = angle * pi / 180;
    float new_h, new_w;

    if(dir == 'h'){
        new_h = h;
        new_w = w + h * fabsf(tanf(angle_rad)) + 1;
    } else { // dir == 'v'
        new_h = h + w * fabsf(tanf(angle_rad)) + 1;
        new_w = w;
    }
    *new_rows = (int) new_h;
    *new_cols = (int) new_w;
}

void
source_coordonnates(char dir, float angle, 
                    float x, float y, 
                    float h, float w, 
                    float* x_src, float* y_src){
    float src_x, src_y;

    Orientation o = compute_orientation(angle);
    float angle_rad = angle * pi / 180;

    if(dir == 'h'){
        switch(o){
            case Right:
                src_x = x - y * tanf(angle_rad);
                src_y = y;
                break;
            case Right_flip:
                src_x = x - (h - y) * tanf(pi - angle_rad);
                src_y = h - y;
                break;
            case Left:
                src_x = x + tanf(angle_rad) * (h - y);
                src_y = y;
                break;
            case Left_flip:
                src_x = w - x - (h - y) * tanf((-1 * pi) + angle_rad);
                src_y = h - y;
            default:
                break;
        }
    } else {    // dir == 'v'
        switch(o){
            case Right:
                src_x = x;
                src_y = y - x * tanf(angle_rad);
                break;
            case Right_flip:
                src_x = w - x;
                src_y = y - (w - x) * tanf(pi - angle_rad);
                break;
            case Left:
                src_x = x;
                src_y = y + tanf(angle_rad) * (w - x);
                break;
            case Left_flip:
                src_x = w - x;
                src_y = h - y - (w - x) * tanf((-1 * pi) + angle_rad);
            default:
                break;
        }
    }

    *x_src = src_x;
    *y_src = src_y;
}

void
process(char dir, float angle, char* ims_name, char* imd_name){

    pnm ims = pnm_load(ims_name);

    int new_rows;
    int new_cols;
    new_image_dimension(dir, angle, 
                        pnm_get_height(ims), pnm_get_width(ims), 
                        &new_rows, &new_cols);

    pnm imd = pnm_new(new_cols, new_rows, PnmRawPpm);  

    for(int i = 0; i < new_rows; i++){
        for(int j = 0; j < new_cols; j++){

            float x_src;
            float y_src;
            source_coordonnates(dir, angle, 
                                (float)j, (float)i, 
                                (float)new_rows, (float)new_cols, 
                                &x_src, &y_src);

            for(int k = 0; k < 3; k++){
                pnm_set_component(imd, i, j, k, bilinear_interpolation(x_src, y_src, ims, k));
            }

        }
    }

    pnm_save(imd, PnmRawPpm, imd_name);

    pnm_free(ims);
    pnm_free(imd);
}

void 
basic_transform(char type, char* ims_name, char* imd_name){

    pnm ims = pnm_load(ims_name);

    int cols = pnm_get_width(ims);
    int rows = pnm_get_height(ims);
    
    pnm imd = pnm_new(cols, rows, PnmRawPpm);

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++){
            for(int k = 0; k < 3; k++){
                switch (type){
                    case 'h':
                        pnm_set_component(imd, i, j, k, pnm_get_component(ims, i, cols - j - 1, k));
                        break;
                    case 'v':
                        pnm_set_component(imd, i, j, k, pnm_get_component(ims, rows - i - 1, j, k));
                        break;
                    case 't':
                        pnm_set_component(imd, i, j, k, pnm_get_component(ims, rows - i - 1, cols - j - 1, k));
                        break;
                    default:
                        break;
                }
            }
        }
    }

    pnm_save(imd, PnmRawPpm, imd_name);

    pnm_free(ims);
    pnm_free(imd);
}

static void usage(char* s){
    fprintf(stderr,"%s <dir>{h ,v} <angle> <ims> <imd>\n",s);
    exit(EXIT_FAILURE);
}

#define PARAM 4
int main(int argc, char* argv[]){
    
    if(argc != PARAM+1) usage(argv[0]);

    char dir = argv[1][0];
    float angle = atof(argv[2]);
    char* ims_name = argv[3];
    char* imd_name = argv[4];

    // Cleaning angle => range [-180 ; 180]

    while(!(-180 <= angle && angle <= 180)){
        if(angle < -180){
            angle += 360;
        } else {
            angle -= 360;
        }
    }

    if(angle == 90 || angle == -90){
        printf("The input angle is invalid (%f). Exiting now...\n", angle);
        exit(EXIT_FAILURE);
    }

    if(angle == 0){
        // Copying the image without any modification
        pnm imd = pnm_load(ims_name);
        pnm_save(imd, PnmRawPpm, imd_name);
        pnm_free(imd);
        return EXIT_SUCCESS;
    }

    if(angle == 180){
        if(dir == 'h'){
            basic_transform('v', ims_name, imd_name);
        } else { // dir == 'v'
            basic_transform('h', ims_name, imd_name);
        }
        return EXIT_SUCCESS;
    }

    if(angle == -180){
        basic_transform('t', ims_name, imd_name);
        return EXIT_SUCCESS;
    }

    process(dir, angle, ims_name, imd_name);

    return EXIT_SUCCESS;
}
