#include <math.h>

#include "domain.h"


enum Case {
    Normal = 0, Right, Left, Up, Bottom, Corner_UR, Corner_UL, Corner_BR, Corner_BL, Outside
};


// Are ij, iij, ijj and iijj in the image
int points_in_image(int i, int j, int rows, int cols){
    return (0 <= i) && (i + 1 < rows) && (0 <= j) && (j + 1 < cols);
}

// Are ij, iij, ijj and iijj outside the image
int points_all_outside(int i, int j, int rows, int cols){
    return (i+1 < 0) || (rows <= i) || (j+1 < 0) || (cols <= j);
}

// Tell which case is it, according to the coordinates. 
// A case tells which of the four points to compute are on the image.
enum Case compute_case(int i, int j, int rows, int cols){
    if(points_in_image(i, j, rows, cols)) return Normal;
    if(points_all_outside(i, j, rows, cols)) return Outside;

    if(i+1 == 0) {
        if(j+1 == 0) return Corner_BR;
        if(j+1 == cols) return Corner_BL;
        return Bottom;

    } else if(i+1 == rows) {
        if(j+1 == 0) return Corner_UR;
        if(j+1 == cols) return Corner_UL;
        return Up;
    } else {
        if(j+1 == 0) return Right;
        return Left;
    }
}


float
get_component_custom(pnm self, int i, int j, pnmChannel channel)
{
    if(i < 0 || j < 0) return 0;

    int rows = pnm_get_height(self);
    int cols = pnm_get_width(self);
    if(i >= rows || j >= cols) return 0;

    return pnm_get_component(self, i, j, channel);;
}


unsigned short 
bilinear_interpolation(float x, float y, pnm ims, int c)
{
    int i = (int) y;
    int j = (int) x;

    float Iij = get_component_custom(ims, i, j, c);
    float Iijj = get_component_custom(ims, i, j + 1, c);
    float Iiij = get_component_custom(ims, i + 1, j, c);
    float Iiijj = get_component_custom(ims, i + 1, j + 1, c);

    int cols = pnm_get_width(ims);
    int rows = pnm_get_height(ims);

    float dx = x - (float) j;
    float dy = y - (float) i;
    
    switch (compute_case(i, j, rows, cols))
    {
        case Normal: // All points are on the image
            return (1 - dx)*(1 - dy)*Iij + dx*(1 - dy)*Iijj + (1 - dx)*dy*Iiij + dx*dy*Iiijj;

        case Right: // Only the points on the right are on the image
            return (1 - dy)*Iijj + dy*Iiijj;
            
        case Left: // Only the points on the left are on the image
            return (1 - dy)*Iij + dy*Iiij;
            
        case Up: // ... and so on ...
            return (1 -dx)*Iij + dx*Iijj;
            
        case Bottom:
            return (1 -dx)*Iiij + dx*Iiijj;
            
        case Corner_UR:
            return Iijj;

        case Corner_UL:
            return Iij;

        case Corner_BR:
            return Iiijj;

        case Corner_BL:
            return Iiij;

        case Outside:
        default:
            return 0;
    }
    
}

