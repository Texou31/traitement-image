#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <bcl.h>

#include "domain.h"

void
process(char* ims_name, char* imd_name){

    pnm ims = pnm_load(ims_name);

    int cols = pnm_get_width(ims);
    int rows = pnm_get_height(ims);

    float h = (float) rows;
    float w = (float) cols;

    pnm imd = pnm_new(cols, rows, PnmRawPpm);

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++){

            float x, y; // Coordonnées de l'image, (0,0) en haut à gauche, dimension (h, w)
            x = (float) j;
            y = (float) i;

            // Coordonnées réduites, (0,0) au centre de l'image, dimension (2,2) où on peut inscrire un cercle de rayon1
            float xp, yp;
            xp = (2 * x / w) - 1;
            yp = 1 - (2 * y / h);
            // xp = (2/w) * x - 1;
            // yp = (2/h) * y - 1;

            float ys, zs; // coordinates of the point on the sphere. The center of the sphere is on the center of the image and have a radius of 1.
            ys = xp;
            zs = yp;

            float rho, theta_rad, psi_rad; //Spheric coordinates 

            rho = 1;
            psi_rad = asinf(zs / rho);
            theta_rad = asinf(ys / (rho * cosf(psi_rad)));

            float x_src = (w/2) * ((theta_rad / pi) + 1);
            float y_src = (h/2) * ((-2* psi_rad / pi) + 1);

            for(int k = 0; k < 3; k++){
                pnm_set_component(imd, i, j, k, bilinear_interpolation(x_src, y_src, ims, k));
            }

        }
    }

    pnm_save(imd, PnmRawPpm, imd_name);

    pnm_free(ims);
    pnm_free(imd);

}

static void usage(char* s){
  fprintf(stderr,"%s <ims> <imd>\n",s);
  exit(EXIT_FAILURE);
}

#define PARAM 2
int main(int argc, char* argv[]){

  if(argc != PARAM+1) usage(argv[0]);

  char* ims_name = argv[1];
  char* imd_name = argv[2];

  process(ims_name, imd_name);
  

  return EXIT_SUCCESS;
}