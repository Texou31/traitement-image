#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <bcl.h>

#include "domain.h"


void
process(int x0, int y0, float angle, char* ims_name, char* imd_name){

    float angle_radian = angle * pi / 180.0; // Put the angle in radians

    pnm ims = pnm_load(ims_name);

    int cols = pnm_get_width(ims);
    int rows = pnm_get_height(ims);

    pnm imd = pnm_new(cols, rows, PnmRawPpm);

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++){

            float ct = cosf(angle_radian);
            float st = sinf(angle_radian);

            float x = (float) j;
            float y = (float) i;

            float x_src = x * ct - y * st + ((float)x0) * (1 - ct) + ((float)y0) * st;
            float y_src = x * st + y * ct - ((float)x0) * st + ((float)y0) * (1 - ct);

            for(int k = 0; k < 3; k++){
                pnm_set_component(imd, i, j, k, bilinear_interpolation(x_src, y_src, ims, k));
            }
        }
    }

    pnm_save(imd, PnmRawPpm, imd_name);

    pnm_free(ims);
    pnm_free(imd);
}

static void usage(char* s){
    fprintf(stderr,"%s <x> <y> <angle> <ims> <imd>\n",s);
    exit(EXIT_FAILURE);
}

#define PARAM 5
int main(int argc, char* argv[]){

    if(argc != PARAM+1) usage(argv[0]);

    int x0 = atoi(argv[1]);
    int y0 = atoi(argv[2]);
    float angle = atoi(argv[3]);
    char* ims_name = argv[4];
    char* imd_name = argv[5];

    process(x0, y0, angle, ims_name, imd_name);

    return EXIT_SUCCESS;
}
