#!/usr/bin/env bash

if [[ ! -e "sphere" ]]; then
    echo "'sphere' does not exist. Trying to compile it."
    make 
    if [[ ! -e "sphere" ]]; then
    echo "'sphere' still does not exist. Exiting now."
    exit 1
    fi
fi

SRC_FILE="earth.ppm"
IMG_WIDTH=512
echo "Source file is $SRC_FILE and have a width of $IMG_WIDTH."

NBR_GIF_FRAMES=50
DELAY=25
echo "The GIF output will have $NBR_GIF_FRAME and a delay of $DELAY."

SPHERE_FILES=()
for ((i = 0 ; i < ${NBR_GIF_FRAMES} ; i++)); do
    DISPLACEMENT=$(($i*$((${IMG_WIDTH}/${NBR_GIF_FRAMES}))))
    ./scroll $DISPLACEMENT 0 $SRC_FILE earth_$DISPLACEMENT.ppm
    ./sphere earth_$DISPLACEMENT.ppm sphere_$DISPLACEMENT.ppm
    rm earth_$DISPLACEMENT.ppm
    SPHERE_FILES+=("sphere_$DISPLACEMENT.ppm")
done

convert -delay $DELAY -loop 0 ${SPHERE_FILES[@]} earth_gif.gif
rm ${SPHERE_FILES[@]}