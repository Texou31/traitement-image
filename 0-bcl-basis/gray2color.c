#include <stdlib.h>
#include <stdio.h>

#include <bcl.h>


void
process(char* ims0, char* ims1,
        char* ims2, char* imd)
{
  pnm src0 = pnm_load(ims0);
  pnm src1 = pnm_load(ims1);
  pnm src2 = pnm_load(ims2);

  pnm dst = pnm_new(pnm_get_width(src0), pnm_get_height(src0), PnmRawPpm);

  unsigned short *channel0 = pnm_get_channel(src0, NULL, 0);
  pnm_set_channel(dst, channel0, 0);
  unsigned short *channel1 = pnm_get_channel(src1, NULL, 1);
  pnm_set_channel(dst, channel1, 1);
  unsigned short *channel2 = pnm_get_channel(src2, NULL, 2);
  pnm_set_channel(dst, channel2, 2);

  pnm_save(dst, PnmRawPpm, imd);

  pnm_free(src0);
  pnm_free(src1);
  pnm_free(src2);
  pnm_free(dst);
  free(channel0);
  free(channel1);
  free(channel2);
}



void 
usage (char *s)
{
  fprintf(stderr,"Usage: %s %s", s, "<ims0> <ims1> <ims2> <imd>\n");
  exit(EXIT_FAILURE);
}

#define PARAM 4
int 
main(int argc, char *argv[])
{
  if (argc != PARAM+1) usage(argv[0]);

  char* ims0 = argv[1];
  char* ims1 = argv[2];
  char* ims2 = argv[3];
  char* imd = argv[4];

  process(ims0, ims1, ims2, imd);


  return EXIT_SUCCESS;
}
