#include <stdlib.h>
#include <stdio.h>

#include <bcl.h>


void
process(char* ims, char* imd)
{
  pnm src = pnm_load(ims);

  size_t rows = pnm_get_height(src);
  size_t cols = pnm_get_width(src);

  pnm dst = pnm_new(cols, rows, PnmRawPpm);


  for(size_t i=0; i< rows; i++){
    for(size_t j=0; j< cols; j++){

      unsigned short sum = 0;
      for(size_t k=0; k<3; k++){
	      sum += pnm_get_component(src, i, j, k);
      }
      const unsigned short value = (short) sum / 3;

      for(size_t k=0; k<3; k++){
	      pnm_set_component(dst, i, j, k, value);
      }

    }
  }

  pnm_save(dst, PnmRawPpm, imd);

  pnm_free(src);
  pnm_free(dst);
}



void 
usage (char *s)
{
  fprintf(stderr,"Usage: %s %s", s, "<ims> <imd>\n");
  exit(EXIT_FAILURE);
}

#define PARAM 2
int 
main(int argc, char *argv[])
{
  if (argc != PARAM+1) usage(argv[0]);

  char* ims = argv[1];
  char* imd = argv[2];

  process(ims, imd);


  return EXIT_SUCCESS;
}
