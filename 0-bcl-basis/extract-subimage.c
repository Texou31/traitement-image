#include <stdlib.h>
#include <stdio.h>

#include <bcl.h>


void
process(const size_t i_start, const size_t j_start,
        const size_t rows, const size_t cols,
        char* ims, char* imd)
{
  pnm src = pnm_load(ims);

  pnm dst = pnm_new(cols, rows, PnmRawPpm);

  for(size_t i=0; i<rows; i++){
    for(size_t j=0; j<cols; j++){
      for(size_t k=0; k<3; k++){
        const unsigned short val = pnm_get_component(src, i + i_start, j + j_start, k);
        pnm_set_component(dst, i, j, k, val);
      }
    }
  }

  pnm_save(dst, PnmRawPpm, imd);
}



void 
usage (char *s)
{
  fprintf(stderr,"Usage: %s %s", s, "<i> <j> <rows> <cols> <ims> <imd>\n");
  exit(EXIT_FAILURE);
}

#define PARAM 6
int 
main(int argc, char *argv[])
{
  if (argc != PARAM+1) usage(argv[0]);

  size_t i_start = atoi(argv[1]);
  size_t j_start = atoi(argv[2]);
  size_t rows = atoi(argv[3]);
  size_t cols = atoi(argv[4]);

  char* ims = argv[5];
  char* imd = argv[6];

  process(i_start, j_start, rows, cols, ims, imd);


  return EXIT_SUCCESS;
}
