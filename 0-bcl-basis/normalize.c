#include <stdlib.h>
#include <stdio.h>

#include <bcl.h>


void 
get_min_max(pnm img, size_t channel,
            unsigned short* min, unsigned short* max){
  
  size_t rows = pnm_get_height(img);
  size_t cols = pnm_get_width(img);

  *min = 256;
  *max = 0;

  for(size_t i=0; i<rows; i++){
    for(size_t j=0; j<cols; j++){
      const unsigned short Iij = pnm_get_component(img, i, j, channel);
      if(*max < Iij) *max = Iij;
      if(*min > Iij) *min = Iij;

    }
  }

}


void
process(size_t min, size_t max,
        char* ims, char* imd)
{
  pnm src = pnm_load(ims);

  size_t rows = pnm_get_height(src);
  size_t cols = pnm_get_width(src);

  pnm dst = pnm_new(cols, rows, PnmRawPpm);

  for(size_t k=0; k<3; k++){

    // Look for MAX(I) and MIN(I)
    unsigned short maxI = 0;
    unsigned short minI = 0;

    get_min_max(src, k, &minI, &maxI);

    // Compute last term of formula
    float first_term = ((float)(max - min))/((float)(maxI - minI));
    float last_term = ((float)(min*maxI - max*minI))/((float)(maxI-minI));
    
    //printf("first term = (max - min) / (maxI - minI) = (%lu - %lu) / (%hu - %hu) = %f \n", 
    //max, min, maxI, minI, first_term);


    printf("k = %lu, Max = %hu, Min = %d, first term = %f, last term = %f \n", k, maxI, minI, first_term, last_term);

    // Apply calcul
    for(size_t i=0; i<rows; i++){
      for(size_t j=0; j<cols; j++){
        const unsigned short Iij = pnm_get_component(src, i, j, k);

        short value = (short) (first_term * ((float)Iij) + last_term);
        if(value < 0) value = 0;
        if(value > 255) value = 255;

        pnm_set_component(dst, i, j, k, value);
      }
    }
  }

  pnm_save(dst, PnmRawPpm, imd);

  pnm_free(src);
  pnm_free(dst);
}



void 
usage (char *s)
{
  fprintf(stderr,"Usage: %s %s", s, "<min> <max> <ims> <imd>\n");
  exit(EXIT_FAILURE);
}

#define PARAM 4
int 
main(int argc, char *argv[])
{
  if (argc != PARAM+1) usage(argv[0]);

  size_t min = atoi(argv[1]);
  size_t max = atoi(argv[2]);
  char* ims = argv[3];
  char* imd = argv[4];

  process(min, max, ims, imd);


  return EXIT_SUCCESS;
}
