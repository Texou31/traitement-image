#include <stdlib.h>
#include <stdio.h>

#include <bcl.h>


void
process(const size_t num,
        char* ims, char* imd)
{
  pnm src = pnm_load(ims);

  pnm dst = pnm_new(pnm_get_width(src), pnm_get_height(src), PnmRawPpm);

  unsigned short *channel = pnm_get_channel(src, NULL, num);
  for(size_t k=0; k<3; k++){
	  pnm_set_channel(dst, channel, k);
  }

  pnm_save(dst, PnmRawPpm, imd);

  pnm_free(src);
  pnm_free(dst);
  free(channel);
}



void 
usage (char *s)
{
  fprintf(stderr,"Usage: %s %s", s, "<num> <ims> <imd>\n");
  exit(EXIT_FAILURE);
}

#define PARAM 3
int 
main(int argc, char *argv[])
{
  if (argc != PARAM+1) usage(argv[0]);

  size_t num = atoi(argv[1]);
  char* ims = argv[2];
  char* imd = argv[3];

  process(num, ims, imd);


  return EXIT_SUCCESS;
}
